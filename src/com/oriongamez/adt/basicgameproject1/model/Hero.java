package com.oriongamez.adt.basicgameproject1.model;

import com.oriongamez.adt.basicgameproject1.model.components.Speed;

import android.graphics.Bitmap;
import android.graphics.Canvas;

public class Hero {

	private Bitmap bitmap;
	private int x;
	private int y;
	private boolean touched;
	private Speed speed;
	
	public Hero(Bitmap bitmap, int x, int y) {
		this.setBitmap(bitmap);
		this.setX(x);
		this.setY(y);
		this.speed = new Speed();
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public boolean isTouched() {
		return touched;
	}

	public void setTouched(boolean touched) {
		this.touched = touched;
	}
	
	public void draw(Canvas canvas) {
		canvas.drawBitmap(bitmap, x - (bitmap.getWidth() / 2), y - (bitmap.getHeight() / 2), null);
	}
	
	public void handleActionDown(int eventX, int eventY) {
		if (eventX >= (x - bitmap.getWidth() / 2) && (eventX <= (x + bitmap.getWidth()/2))) {
			if (eventY >= (y - bitmap.getWidth() / 2) && (eventY <= (y + bitmap.getWidth()/2))) {
				setTouched(true);
			} else {
				setTouched(false);
			}
		}
	}

	public Speed getSpeed() {
		return speed;
	}

	public void setSpeed(Speed speed) {
		this.speed = speed;
	}
	
	public void update() {
		if (!touched) {
			x += (speed.getXv() * speed.getxDirection());
			y += (speed.getYv() * speed.getyDirection());
		}
	}
	
	
}
