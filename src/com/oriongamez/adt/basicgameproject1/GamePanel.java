package com.oriongamez.adt.basicgameproject1;

import com.oriongamez.adt.basicgameproject1.model.Hero;
import com.oriongamez.adt.basicgameproject1.model.components.Speed;

import android.R.color;
import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.SurfaceHolder.Callback;

public class GamePanel extends SurfaceView implements Callback {

	public static final String TAG = "SZYDLOWSKI.JACEK.GamePanel";
	
	private GameThread thread;
	private Hero hero;

	private String avgFps;
	
	public GamePanel(Context context) {
		super(context);
		getHolder().addCallback(this);

		hero = new Hero(BitmapFactory.decodeResource(getResources(), R.drawable.png1), 150, 150);
		
		thread = new GameThread(getHolder(), this);
		
		setFocusable(true);
		
		Log.i(TAG, "onGamePanel C-tor");
	}

	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub

	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		thread.setRunning(true);
		thread.start();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				// try again
			}
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			hero.handleActionDown((int)event.getX(), (int)event.getY());
			
			if (event.getY() > getHeight() - 50) {
				thread.setRunning(false);
				((Activity) getContext()).finish();
			} else {
				Log.i(TAG, "Touch X: " + event.getX() + " Y: " + event.getY());
			}
		}
		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			if (hero.isTouched()) {
				hero.setX((int)event.getX());
				hero.setY((int)event.getY());
			}
		}
		if (event.getAction() == MotionEvent.ACTION_UP) {
			hero.setTouched(false);
		}
		
		return true;
//		return super.onTouchEvent(event);
	}

	private void displayFps(Canvas canvas, String fps) {
		if (canvas != null && fps != null) {
			Paint paint = new Paint();
			paint.setARGB(255, 255, 255, 255);
			canvas.drawText(fps, this.getWidth() - 50, 20, paint);
		}
	}
	
	
	public void render(Canvas canvas) {
		canvas.drawColor(Color.DKGRAY);
		hero.draw(canvas);	
		// fps
		displayFps(canvas, getAvgFps());
	}

	public void update() {
		// check collision with right wall if heading right
		if (hero.getSpeed().getxDirection() == Speed.DIRECTION_RIGHT
				&& hero.getX() + hero.getBitmap().getWidth() / 2 >= getWidth()) {
			hero.getSpeed().toggleXDirection();
		}
		// check collision with left wall if heading left
		if (hero.getSpeed().getxDirection() == Speed.DIRECTION_LEFT
				&& hero.getX() - hero.getBitmap().getWidth() / 2 <= 0) {
			hero.getSpeed().toggleXDirection();
		}
		// check collision with bottom wall if heading down
		if (hero.getSpeed().getyDirection() == Speed.DIRECTION_DOWN
				&& hero.getY() + hero.getBitmap().getHeight() / 2 >= getHeight()) {
			hero.getSpeed().toggleYDirection();
		}
		// check collision with top wall if heading up
		if (hero.getSpeed().getyDirection() == Speed.DIRECTION_UP
				&& hero.getY() - hero.getBitmap().getHeight() / 2 <= 0) {
			hero.getSpeed().toggleYDirection();
		}
		// Update the lone droid
		hero.update();
	}

	public String getAvgFps() {
		return avgFps;
	}

	public void setAvgFps(String avgFps) {
		this.avgFps = avgFps;
	}

	
	
}
